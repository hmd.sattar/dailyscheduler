import 'dart:async';
import 'dart:io';

import 'package:dailyscheduler/model/Category.dart';
import 'package:dailyscheduler/model/Event.dart';
import 'package:dailyscheduler/model/Task.dart';
import 'package:dailyscheduler/model/TaskDone.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DbHelper {
  static final DbHelper _instance = new DbHelper.internal();

  factory DbHelper() => _instance;

  final String tbl_Task = "Tbl_Task";
  final String TaskId = "TaskId";
  final String Name = "Name";
  final String ScheduleRepeat = "ScheduleRepeat";
  final String GoalCount = "GoalCount";
//  final String CatId = "CatId";

  final String tbl_TaskDone = "tbl_TaskDone";
  final String DoneId = "DoneId";
//  final String TaskId = "TaskId";
  final String DoneDate = "DoneDate";

  final String tbl_Category = "tbl_Category";
  final String CatId = "CatId";
  final String CatName = "CatName";

  final String tbl_Event = "tbl_Event";
  final String eventId = "EventId";
  final String eventName = "EventName";
  final String eventDate = "EventDate";
  final String eventTime = "EventTime";
  final String eventDone = "EventDone";

  static Database _db;

  Future<Database> get db async {
    if (_db == null) _db = await initDb();

    return _db;
  }

  DbHelper.internal();

  initDb() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    //home://directory/files/maindb.db
    String path = join(documentDirectory.path, "maindb.db");

    var ourDb = await openDatabase(path,
        version: 3, onCreate: _onCreate, onUpgrade: _onUpgrade);
    return ourDb;
  }

  void _onCreate(Database db, int version) async {
    String CREATE_TABLE_Task =
        "CREATE TABLE $tbl_Task($TaskId INTEGER PRIMARY KEY,"
        "$Name TEXT, $ScheduleRepeat INTEGER, $GoalCount INTEGER, $CatId INTEGER DEFAULT 1)";
    String CREATE_TABLE_TaskDONE =
        "CREATE TABLE $tbl_TaskDone($DoneId INTEGER PRIMARY KEY,"
        "$CatId INTEGER, $TaskId INTEGER, $DoneDate TEXT)";
    String CREATE_TABLE_Category =
        "CREATE TABLE $tbl_Category($CatId INTEGER PRIMARY KEY, $CatName TEXT)";
    String CREATE_TABLE_EVENT =
        "CREATE TABLE $tbl_Event($eventId INTEGER PRIMARY KEY,"
        "$eventName TEXT, $eventDate TEXT, $eventTime TEXT, $eventDone bool)";

    await db.execute(CREATE_TABLE_Task);
    await db.execute(CREATE_TABLE_TaskDONE);
    await db.execute(CREATE_TABLE_Category);
    await db.execute(CREATE_TABLE_EVENT);

    var model = Category(CatName: "سایر");
    await db.insert(tbl_Category, model.toMap());
  }

  void _onUpgrade(Database db, int oldVersion, int version) async {
    if (oldVersion < 2) {
      String CREATE_TABLE_EVENT =
          "CREATE TABLE $tbl_Event($eventId INTEGER PRIMARY KEY,"
          "$eventName TEXT, $eventDate TEXT, $eventTime TEXT, $eventDone bool)";
      await db.execute(CREATE_TABLE_EVENT);
    } else if (oldVersion < 3) {
      String CREATE_TABLE_Category =
          "CREATE TABLE $tbl_Category($CatId INTEGER PRIMARY KEY, $CatName TEXT)";
      await db.execute(CREATE_TABLE_Category);
      await db
          .execute("ALTER TABLE $tbl_Task ADD COLUMN $CatId INTEGER DEFAULT 1");

      var model = Category(CatName: "سایر");
      await db.insert(tbl_Category, model.toMap());
    }
  }

  Future<int> saveTask(Task model) async {
    var db_c = await db;

    return await db_c.insert(tbl_Task, model.toMap());
  }

  Future<int> editTask(Task model) async {
    var db_c = await db;

    return await db_c.update(tbl_Task, model.toMap(),
        where: "$TaskId = ?", whereArgs: [model.TaskId]);
  }

  Future<int> deleteTask(Task model) async {
    var db_c = await db;

    var resDel = await db_c
        .delete(tbl_Task, where: "$TaskId = ?", whereArgs: [model.TaskId]);
    if (resDel > 0)
      await db_c.delete(tbl_TaskDone,
          where: "$TaskId = ?", whereArgs: [model.TaskId]);

    return resDel;
  }

  Future<List<Map<String, dynamic>>> getAllTasks() async {
    var db_c = await db;

    return await db_c.rawQuery("SELECT *, (SELECT $CatName FROM $tbl_Category "
        "WHERE $tbl_Category.$CatId = $tbl_Task.$CatId) AS CatName FROM $tbl_Task");
  }

  Future<List<Map<String, dynamic>>> getAllTaskDoneByDate(date) async {
    var db_c = await db;

    return await db_c
        .rawQuery("SELECT * FROM $tbl_TaskDone WHERE $DoneDate == '$date'");
  }

  Future<List<Map<String, dynamic>>> getAllTaskDoneByDateToDate(
      fromDate, toDate) async {
    var db_c = await db;

    return await db_c.rawQuery(
        "SELECT * FROM $tbl_TaskDone WHERE $DoneDate >= '$fromDate' AND $DoneDate <= '$toDate'");
  }

  Future<int> saveTaskDone(TaskDone model) async {
    var db_c = await db;

    return await db_c.insert(tbl_TaskDone, model.toMap());
  }

  Future<int> removeTaskDone(TaskDone model) async {
    var db_c = await db;

    return await db_c.delete(tbl_TaskDone,
        where: "$TaskId = ? AND $DoneDate = ?",
        whereArgs: [model.TaskId, model.DoneDate]);
  }

  Future<int> saveEvent(Event model) async {
    var db_c = await db;

    return await db_c.insert(tbl_Event, model.toMap());
  }

  Future<List<Map<String, dynamic>>> getAllEvents() async {
    var db_c = await db;

    return await db_c.rawQuery("SELECT * FROM $tbl_Event");
  }

  Future<List<Map<String, dynamic>>> getAllEventByDate(date) async {
    var db_c = await db;

    return await db_c
        .rawQuery("SELECT * FROM $tbl_Event WHERE $eventDate == '$date'");
  }

  Future<int> updateEvent(Event model) async {
    var db_c = await db;

    return await db_c.update(tbl_Event, model.toMap(),
        where: "$eventId = ?", whereArgs: [model.EventId]);
  }

  Future<int> deleteEvent(Event model) async {
    var db_c = await db;

    var resDel = await db_c
        .delete(tbl_Event, where: "$eventId = ?", whereArgs: [model.EventId]);

    return resDel;
  }

  Future<int> saveCategory(Category model) async {
    var db_c = await db;

    return await db_c.insert(tbl_Category, model.toMap());
  }

  Future<int> removeCategory(Category model) async {
    var db_c = await db;

    return await db_c
        .delete(tbl_Category, where: "$CatId = ?", whereArgs: [model.CatId]);
  }

  Future<List<Map<String, dynamic>>> getCategory({int catId = 0}) async {
    var db_c = await db;

    return await db_c.rawQuery(
        "SELECT $CatId, $CatName, (SELECT COUNT(*) FROM $tbl_Task WHERE $tbl_Task.$CatId = $tbl_Category.$CatId) AS TasksCount FROM $tbl_Category "
        "WHERE $catId = 0 OR $CatId = $catId ");
  }
}
