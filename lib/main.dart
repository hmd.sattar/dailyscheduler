import 'package:dailyscheduler/ui/Home.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      builder: (BuildContext context, Widget child) {
        return Directionality(
            textDirection: TextDirection.rtl,
            child: Builder(builder: (BuildContext context) {
              return MediaQuery(
                  data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                  child: child);
            }));
      },
      theme: ThemeData(fontFamily: 'BYekan'),
      home: Home(),
    );
  }
}
