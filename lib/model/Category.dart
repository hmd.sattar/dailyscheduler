class Category {
  Category({this.CatName, this.CatId});

  int CatId;
  String CatName;

  int TasksCount;

  Category.fromMap(Map<String, dynamic> map) {
    this.CatId = map["CatId"];
    this.CatName = map["CatName"];
    this.TasksCount = map["TasksCount"];
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map["CatName"] = this.CatName;
    if (CatId != null) map["CatId"] = this.CatId;

    return map;
  }
}
