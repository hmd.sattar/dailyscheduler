class Event {
  Event(
      {this.EventId,
      this.EventName,
      this.EventDate,
      this.EventTime,
      this.EventDone});

  int EventId;
  String EventName;
  String EventDate;
  String EventTime;
  bool EventDone;

  Event.fromMap(Map<String, dynamic> map) {
    this.EventId = map["EventId"];
    this.EventName = map["EventName"];
    this.EventDate = map["EventDate"];
    this.EventTime = map["EventTime"];
    this.EventDone = map["EventDone"] == 1;
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();

    map["EventName"] = this.EventName;
    map["EventDate"] = this.EventDate;
    map["EventTime"] = this.EventTime;
    map["EventDone"] = this.EventDone ? 1 : 0;

    if (EventId != null) map["EventId"] = this.EventId;

    return map;
  }
}
