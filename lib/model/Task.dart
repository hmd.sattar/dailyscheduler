class Task {
  Task(
      {this.TaskId,
      this.Name,
      this.ScheduleRepeat,
      this.GoalCount,
      this.CatId});

  int TaskId;
  String Name;
  int ScheduleRepeat;
  int GoalCount;
  int DoneCount;
  int DonePercentage;
  bool dones;
  int CatId;
  String CatName;

  Task.fromMap(Map<String, dynamic> map) {
    this.TaskId = map["TaskId"];
    this.Name = map["Name"];
    this.ScheduleRepeat = map["ScheduleRepeat"];
    this.GoalCount = map["GoalCount"];

    this.CatId = map["CatId"];
    this.CatName = map["CatName"];
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();

    map["Name"] = this.Name;
    map["ScheduleRepeat"] = this.ScheduleRepeat;
    map["GoalCount"] = this.GoalCount;
    map["CatId"] = this.CatId;
//    map["CatName"] = this.CatName;

    if (TaskId != null) map["TaskId"] = this.TaskId;

    return map;
  }
}
