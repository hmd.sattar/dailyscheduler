class TaskDone {
  TaskDone({this.TaskId, this.DoneDate});

  int DoneId;
  int TaskId;
  String DoneDate;

  TaskDone.fromMap(Map<String, dynamic> map) {
    this.TaskId = map["TaskId"];
    this.DoneId = map["DoneId"];
    this.DoneDate = map["DoneDate"];
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();

    map["TaskId"] = this.TaskId;
    map["DoneDate"] = this.DoneDate;

    if (DoneId != null) map["DoneId"] = this.DoneId;

    return map;
  }
}
