import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:meta/meta.dart';

NotificationDetails get _noSound {
  final androidChannelSpecifics = AndroidNotificationDetails(
    'silent channel id',
    'silent channel name',
    'silent channel description',
    playSound: false,
  );
  final iOSChannelSpecifics = IOSNotificationDetails(presentSound: false);

  return NotificationDetails(androidChannelSpecifics, iOSChannelSpecifics);
}

Future showSilentNotification(
  FlutterLocalNotificationsPlugin notifications, {
  @required String title,
  @required String body,
  int id = 0,
}) =>
    _showNotification(notifications,
        title: title, body: body, id: id, type: _noSound);

NotificationDetails get _ongoing {
  final androidChannelSpecifics = AndroidNotificationDetails(
    'your channel id',
    'your channel name',
    'your channel description',
    importance: Importance.Max,
    priority: Priority.High,
    ongoing: false,
    autoCancel: false,
  );
  final iOSChannelSpecifics = IOSNotificationDetails();
  return NotificationDetails(androidChannelSpecifics, iOSChannelSpecifics);
}

NotificationDetails get _scheduler {
  final androidChannelSpecifics =
      AndroidNotificationDetails('0', 'scheduler', 'body');
  final iOSChannelSpecifics = IOSNotificationDetails();
  return NotificationDetails(androidChannelSpecifics, iOSChannelSpecifics);
}

Future showOngoingNotification(
  FlutterLocalNotificationsPlugin notifications, {
  @required String title,
  @required String body,
  int id = 0,
}) =>
    _showNotification(notifications,
        title: title, body: body, id: id, type: _ongoing);

Future _showNotification(
  FlutterLocalNotificationsPlugin notifications, {
  @required String title,
  @required String body,
  @required NotificationDetails type,
  int id = 0,
}) =>
    notifications.show(id, title, body, type);

Future scheduleNotification(
  FlutterLocalNotificationsPlugin notifications, {
  @required String title,
  @required String body,
  @required DateTime schedule,
  int id = 0,
}) async {
  try {
    await _scheduleNotification(notifications,
        title: title, body: body, id: id, type: _ongoing, schedule: schedule);
  } catch (e) {
    return throw Exception(e.toString() + "خطا در ثبت نوتیفیکیشن");
  }
}

Future _scheduleNotification(
  FlutterLocalNotificationsPlugin notifications, {
  @required String title,
  @required String body,
  @required NotificationDetails type,
  @required DateTime schedule,
  int id = 0,
}) async {
  try {
    final androidChannelSpecifics =
        AndroidNotificationDetails(id.toString(), title, body);
    final iOSChannelSpecifics = IOSNotificationDetails();
    var channel =
        NotificationDetails(androidChannelSpecifics, iOSChannelSpecifics);
    return await notifications.schedule(id, title, body, schedule, channel);
  } catch (e) {
    return throw Exception(e.toString() + "خطا در ثبت نوتیفیکیشن");
  }
}
