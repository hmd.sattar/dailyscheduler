import 'package:flutter/material.dart';

class AboutUs extends StatefulWidget {
  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        builder: (BuildContext context, Widget child) {
          return Directionality(
              textDirection: TextDirection.rtl,
              child: Builder(builder: (BuildContext context) {
                return MediaQuery(
                    data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                    child: child);
              }));
        },
        theme: ThemeData(fontFamily: 'BYekan'),
        home: Scaffold(
//        key: _scaffoldKey,
          appBar: AppBar(
            title: Text("درباره ما"),
            leading: IconButton(
                icon: Icon(Icons.close), onPressed: () => _onWillPop()),
          ),
          body: ListView(
            children: <Widget>[
              SizedBox(height: 35),
              Center(
                child: Text("گروه نرم افزاری دارت",
                    style:
                        TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
              ),
              SizedBox(height: 30),
              Center(
                child: Text(
                    "برنامه نویسی انواع اپلیکیشن های موبایلی (Android & IOS)"),
              ),
              SizedBox(height: 15),
              Center(
                child: Text("برنامه نویسی انواع وب سایت و وب اپلیکیشن"),
              ),
              SizedBox(height: 15),
              Center(
                child: Text("و ..."),
              ),
              SizedBox(height: 15),
              Center(
                child: Text("آدرس: یزد، بلوار مدرس، طبقه فوقانی بانک تجارت"),
              ),
              SizedBox(height: 15),
              Center(
                child: Text("تلفن: 36282131-035"),
              ),
              SizedBox(height: 15),
              Center(
                child: Text(""),
              ),
            ],
          ),
        ));
  }

  _onWillPop() {
    return Navigator.pop(context);
  }
}
