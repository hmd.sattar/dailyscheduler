//import 'package:dailyschedule/database/DBHelper.dart';
//import 'package:dailyschedule/ui/Home.dart';
//import 'package:flutter/material.dart';
//import 'package:flutter_local_notifications/flutter_local_notifications.dart';
//import 'package:modal_progress_hud/modal_progress_hud.dart';
//
//class AlarmSetting extends StatefulWidget {
//  @override
//  _AlarmSettingState createState() => _AlarmSettingState();
//}
//
//class _AlarmSettingState extends State<AlarmSetting> {
//  final notifications = FlutterLocalNotificationsPlugin();
//  final _scaffoldKey = GlobalKey<ScaffoldState>();
//
//  bool _isSaving = false;
//  bool _isDailySet = false;
//  bool _isWeeklySet = false;
//  bool _isMonthlySet = false;
//
//  var dailyTime = TextEditingController();
//  var weeklyTime = TextEditingController();
//  var monthlyTime = TextEditingController();
//
//  DbHelper db;
//
//  @override
//  void initState() {
//    super.initState();
//
//    final settingsAndroid =
//        AndroidInitializationSettings('@mipmap/ic_launcher');
//    final settingsIOS = IOSInitializationSettings(
//        onDidReceiveLocalNotification: (id, title, body, payload) =>
//            onSelectNotification(payload));
//
//    notifications.initialize(
//        InitializationSettings(settingsAndroid, settingsIOS),
//        onSelectNotification: onSelectNotification);
//
//    initialAlarms();
//  }
//
//  Future onSelectNotification(String payload) async => await Navigator.push(
//        context,
//        MaterialPageRoute(builder: (context) => Home()),
//      );
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      key: _scaffoldKey,
//      appBar: AppBar(
//        title: Text("تنظیمات یادآور"),
//        leading: IconButton(
//            icon: Icon(Icons.arrow_back), onPressed: () => _onWillPop()),
//      ),
//      body: ModalProgressHUD(
//        inAsyncCall: _isSaving,
//        child: Container(
//            color: Colors.grey[300],
//            child: Container(
////              decoration: BoxDecoration(
////                  color: Colors.white,
////                  borderRadius: BorderRadius.all(Radius.circular(3)),
////                  boxShadow: [
////                    BoxShadow(
////                        color: Colors.grey,
////                        blurRadius: 0.5,
////                        spreadRadius: 0.5,
////                        offset: Offset(-0.2, 0.4))
////                  ]),
//              child: ListView(
//                padding: const EdgeInsets.all(10.0),
//                children: <Widget>[],
//              ),
//            )),
//      ),
//    );
//  }
//
//  _onWillPop() {
//    return Navigator.pop(context);
//  }
//
//  Future initialAlarms() async {
//    var notify = await notifications.pendingNotificationRequests();
//
//    if (notify.any((a) => a.id == 10010)) {
//      setState(() {
//        _isDailySet = true;
//        dailyTime.text = notify.firstWhere((w) => w.id == 10010).body;
//      });
//    }
//    if (notify.any((a) => a.id == 10020)) {
//      setState(() {
//        _isWeeklySet = true;
//        weeklyTime.text = notify.firstWhere((w) => w.id == 10020).body;
//      });
//    }
//  }
//}
