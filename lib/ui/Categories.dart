import 'package:dailyscheduler/database/DBHelper.dart';
import 'package:dailyscheduler/model/Category.dart';
import 'package:dailyscheduler/ui/Component/DialogBox.dart';
import 'package:flutter/material.dart';

class Categories extends StatefulWidget {
  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final boxShadow = [
    BoxShadow(
        color: Colors.grey,
        blurRadius: 1,
        offset: Offset(0.8, 1),
        spreadRadius: 0.5)
  ];

  TextEditingController catName = TextEditingController();
  List<Category> _categories;
  DbHelper _db;

  @override
  void initState() {
    super.initState();

    _categories = List();

    initCategories();
  }

  Future initCategories() async {
    _db = await DbHelper();

    var res = await _db.getCategory();
    var cats = res.map((item) => Category.fromMap(item)).toList();
    setState(() {
      _categories = cats;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("مدیریت دسته بندی ها"),
      ),
      backgroundColor: Colors.grey[100],
      body: Container(
        padding: EdgeInsets.all(15),
        child: Column(
          children: <Widget>[
            headerTable(),
            Expanded(child: contentTable()),
            newCategoryForm()
          ],
        ),
      ),
    );
  }

  headerTable() {
    return Container(
      height: 30,
      decoration: BoxDecoration(
          color: Colors.grey[400],
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(3), topLeft: Radius.circular(3)),
          boxShadow: boxShadow),
      child: Row(
        children: <Widget>[
          Expanded(flex: 1, child: Text("#", textAlign: TextAlign.center)),
          Expanded(flex: 9, child: Text("دسته بندی")),
          Expanded(flex: 2, child: Text("فعالیت", textAlign: TextAlign.center)),
          Expanded(flex: 1, child: Text("حذف", textAlign: TextAlign.center)),
        ],
      ),
    );
  }

  contentTable() {
    return Container(
      child: ListView.separated(
          itemBuilder: (BuildContext context, int index) {
            return categoryCard(
                _categories[index], index + 1, index + 1 == _categories.length);
          },
          separatorBuilder: (BuildContext context, int index) {
            return SizedBox(height: 0.2);
          },
          itemCount: _categories.length),
    );
  }

  categoryCard(Category cat, int row, bool lastItem) {
    return Container(
      height: 30,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(lastItem ? 5 : 0),
            bottomLeft: Radius.circular(lastItem ? 5 : 0)),
        boxShadow: boxShadow,
      ),
      child: Row(
        children: <Widget>[
          Expanded(
              flex: 1,
              child: Text(row.toString(), textAlign: TextAlign.center)),
          Expanded(flex: 9, child: Text(cat.CatName)),
          Expanded(
              flex: 2,
              child:
                  Text(cat.TasksCount.toString(), textAlign: TextAlign.center)),
          Expanded(
              flex: 1,
              child: InkWell(
                  child: Icon(
                    Icons.delete,
                    color: cat.TasksCount > 0
                        ? Colors.grey[300]
                        : Colors.redAccent,
                  ),
                  onTap: () => delCat(cat))),
        ],
      ),
    );
  }

  newCategoryForm() {
    return Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(3)),
            boxShadow: boxShadow),
        margin: EdgeInsets.only(bottom: 5, right: 10, left: 10),
        child: Column(
          children: <Widget>[
            Container(
                decoration: BoxDecoration(
                    color: Colors.grey[400],
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(3),
                        topRight: Radius.circular(3))),
                width: double.maxFinite,
                padding: EdgeInsets.all(10),
                child: Text("ثبت دسته بندی جدید")),
            Container(
              padding: EdgeInsets.only(left: 5, right: 10, top: 5, bottom: 5),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      controller: catName,
                      decoration: InputDecoration(hintText: "نام دسته بندی"),
                    ),
                  ),
                  IconButton(
                    color: Colors.lightBlue,
                    iconSize: 30,
                    icon: Icon(Icons.save),
                    onPressed: () => saveCategory(),
                  )
                ],
              ),
            ),
          ],
        ));
  }

  saveCategory() async {
    var name = catName.text;
    if (name == null || name.trim() == "") {
      _scaffoldKey.currentState.showSnackBar(
          SnackBar(content: Text("نام دسته بندی جدید را وارد نمایید")));
      catName.text = "";
      return;
    }
    var model = Category(CatName: name);

    var res = await _db.saveCategory(model);

    if (res > 0) {
      model.CatId = res;
      model.TasksCount = 0;
      catName.text = "";
      setState(() {
        _categories.add(model);
      });
    } else {
      _scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text("خطا در ثبت اطلاعات")));
    }
  }

  delCat(Category model) async {
    if (model.TasksCount > 0) {
      _scaffoldKey.currentState.showSnackBar(
          SnackBar(content: Text("این دسته بندی دارای فعالیت می باشد")));
      return;
    }

    var val = await showDialog(
        context: context,
        child: DialogBox(
          title: "حذف دسته بندی",
          question: "آیا از حذف این دسته بندی اطمینان دارید؟",
        ));
    if (val == null || !val) {
      return;
    }

    if (_db == null) _db = DbHelper();
    var res = await _db.removeCategory(model);
    if (res <= 0) {
      _scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text("خطا در حذف دسته بندی")));
      return;
    }
    setState(() {
      _categories.remove(model);
    });
  }
}
