import 'package:flutter/material.dart';

class ActionsDialogBox extends StatefulWidget {
  ActionsDialogBox({Key key, this.actions}) : super(key: key);

//  final String title;
//  final String question;
  final List<listTileModel> actions;

  @override
  _ActionsDialogBoxState createState() => _ActionsDialogBoxState();
}

class _ActionsDialogBoxState extends State<ActionsDialogBox> {
  List<listTileModel> actions;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.all(0),
      content: ConstrainedBox(
          constraints:
              BoxConstraints(maxHeight: 57.0 * actions.length, minHeight: 50),
          child: Container(
            color: Colors.grey[300],
            width: double.maxFinite,
            child: ListView(
              children: List.generate(
                actions.length,
                (int index) => Container(
                      margin: index == actions.length - 1
                          ? EdgeInsets.only(bottom: 0)
                          : EdgeInsets.only(bottom: 1),
                      color: Colors.white,
                      child: Directionality(
                        child: ListTile(
                          title: Text(actions[index].title,
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: "BYekan",
                                  fontStyle: FontStyle.normal)),
                          leading: actions[index].icon,
                          onTap: () {
                            Navigator.of(context).pop(actions[index].result);
                          },
                        ),
                        textDirection: TextDirection.rtl,
                      ),
                    ),
//            children: <Widget>[
//              Text('Item 1'),
//              Text('Item 2'),
//              Text('Item 3'),
//            ],
              ),
            ),
          )
//          Column(
//              mainAxisAlignment: MainAxisAlignment.center,
//              textDirection: TextDirection.ltr,
//              children: List.generate(actions.length, (int index) {
//                return actions[index];
//              }))
          ),
    );
  }

  @override
  void initState() {
    super.initState();

    actions = widget.actions;
//    if (buttons == null)
//      buttons = [
//        new FlatButton(
//          child: new Text("بله"),
//          onPressed: () {
//            Navigator.of(context).pop(true);
//          },
//        ),
//        new FlatButton(
//          child: new Text("انصراف"),
//          onPressed: () {
//            Navigator.of(context).pop(false);
//          },
//        ),
//      ];
  }
}

class listTileModel {
  final String title;
  final int result;
  final Icon icon;

  listTileModel({this.title, this.result, this.icon});
}
