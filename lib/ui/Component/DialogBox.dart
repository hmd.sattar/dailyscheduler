import 'package:flutter/material.dart';

class DialogBox extends StatefulWidget {
  DialogBox({Key key, this.question = "", this.title = "", this.buttons})
      : super(key: key);

  final String title;
  final String question;
  final List<Widget> buttons;

  @override
  _DialogBoxState createState() => _DialogBoxState();
}

class _DialogBoxState extends State<DialogBox> {
  List<Widget> buttons;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: widget.title == ""
            ? Container()
            : Text(
                widget.title,
                textDirection: TextDirection.rtl,
                style: TextStyle(
                    height: 1.5,
                    fontSize: 16,
                    fontFamily: "BYekan",
                    fontStyle: FontStyle.normal),
              ),
        content: Text(widget.question,
            textDirection: TextDirection.rtl,
            style: TextStyle(
                fontSize: 14,
                fontFamily: "BYekan",
                fontStyle: FontStyle.normal)),
        actions: buttons);
  }

  @override
  void initState() {
    super.initState();

    buttons = widget.buttons;
    if (buttons == null)
      buttons = [
        new FlatButton(
          child: new Text("بله"),
          onPressed: () {
            Navigator.of(context).pop(true);
          },
        ),
        new FlatButton(
          child: new Text("انصراف"),
          onPressed: () {
            Navigator.of(context).pop(false);
          },
        ),
      ];
  }
}
