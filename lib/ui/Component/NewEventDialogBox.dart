import 'package:dailyscheduler/ui/Component/ActionsDialogBox.dart';
import 'package:flutter/material.dart';

class NewEventDialogBox extends StatefulWidget {
  NewEventDialogBox({Key key, this.actions}) : super(key: key);

//  final String title;
//  final String question;
  final List<listTileModel> actions;

  @override
  _NewEventDialogBoxState createState() => _NewEventDialogBoxState();
}

class _NewEventDialogBoxState extends State<NewEventDialogBox> {
  List<listTileModel> actions;

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints:
          BoxConstraints(maxHeight: 57.0 * actions.length, minHeight: 50),
      child: Container(
        color: Colors.grey[300],
        width: double.maxFinite,
        child: Form(
          child: ListView(children: [
            Container(
                margin: EdgeInsets.only(left: 10, right: 10),
                child: TextFormField(
                  textDirection: TextDirection.rtl,
                  decoration: InputDecoration(hintText: "نام رویداد"),
                )),
          ]),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    actions = widget.actions;
//    if (buttons == null)
//      buttons = [
//        new FlatButton(
//          child: new Text("بله"),
//          onPressed: () {
//            Navigator.of(context).pop(true);
//          },
//        ),
//        new FlatButton(
//          child: new Text("انصراف"),
//          onPressed: () {
//            Navigator.of(context).pop(false);
//          },
//        ),
//      ];
  }
}
