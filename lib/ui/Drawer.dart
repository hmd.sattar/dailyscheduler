import 'package:dailyscheduler/ui/AboutUs.dart';
import 'package:dailyscheduler/ui/Categories.dart';
import 'package:dailyscheduler/ui/RegisterNewTask.dart';
import 'package:dailyscheduler/ui/Report.dart';
import 'package:dailyscheduler/ui/ReportByDate.dart';
import 'package:flutter/material.dart';

class MyDrawer extends StatefulWidget {
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            child: Image(image: AssetImage("assets/icon.png")),
          ),
          ListTile(
            title: Text("خانه"),
            leading: Icon(Icons.home),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: Text("ثبت کار جدید"),
            leading: Icon(Icons.work),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => RegisterNewTask()));
            },
          ),
          ListTile(
            title: Text("دسته بندی فعالیت ها"),
            leading: Icon(Icons.category),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Categories()));
            },
          ),
          ListTile(
            title: Text("گزارش فعالیت ها"),
            leading: Icon(Icons.report),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Report()));
            },
          ),
          ListTile(
            title: Text("گزارش با تاریخ"),
            leading: Icon(Icons.date_range),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ReportByDate()));
            },
          ),
          Divider(),
//          ListTile(
//            title: Text("تنظیمات یادآور"),
//            leading: Icon(Icons.alarm),
//            onTap: () {
//              Navigator.pop(context);
//              Navigator.push(context,
//                  MaterialPageRoute(builder: (context) => AlarmSetting()));
//            },
//          ),
          ListTile(
            title: Text("درباره ما"),
            leading: Icon(Icons.info_outline),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => AboutUs()));
            },
          ),
        ],
      ),
    );
  }
}
