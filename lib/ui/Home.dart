import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:dailyscheduler/database/DBHelper.dart';
import 'package:dailyscheduler/model/Category.dart';
import 'package:dailyscheduler/model/Event.dart';
import 'package:dailyscheduler/model/Task.dart';
import 'package:dailyscheduler/model/TaskDone.dart';
import 'package:dailyscheduler/notification/Notification_Helper.dart';
import 'package:dailyscheduler/ui/Component/ActionsDialogBox.dart';
import 'package:dailyscheduler/ui/Component/DialogBox.dart';
import 'package:dailyscheduler/ui/Drawer.dart';
import 'package:dailyscheduler/ui/EditTask.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:path_provider/path_provider.dart';
import 'package:persian_datepicker/persian_datepicker.dart';
import 'package:shamsi_date/shamsi_date.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  List<Task> tasks = List();
  List<Event> events = List();
  DbHelper db;
  PersianDatePickerWidget persianDatePicker;
  final notifications = FlutterLocalNotificationsPlugin();
  final dateController = TextEditingController();
  final timeController = TextEditingController();
  final eventName = TextEditingController();
  final dayName = TextEditingController();
  final longDate = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TabController _tabController;

  bool _showNewEventForm = false;
  bool _inEventTab = false;
  bool _reminderChecked = false;
  int daily = 1;
  int weekly = 7;
  int mountly = 30;

  List<Category> categories = List();

  final boxShadow = BoxShadow(
      color: Colors.grey,
      blurRadius: 0.5,
      spreadRadius: 0.5,
      offset: Offset(-0.2, 0.4));

  @override
  void initState() {
    super.initState();
    db = DbHelper();

    final settingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    final settingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: (id, title, body, payload) =>
            onSelectNotification(payload));

    notifications.initialize(
        InitializationSettings(settingsAndroid, settingsIOS),
        onSelectNotification: onSelectNotification);

    _tabController = TabController(length: 2, vsync: this, initialIndex: 0);
    _tabController.addListener(() {
      setState(() {
        _inEventTab = _tabController.index == 1;
      });
    });
    persianDatePicker =
        PersianDatePicker(controller: dateController, fontFamily: 'BYekan'
//      datetime: '1397/06/09',
                )
            .init();

    dateController.text = replaceDate(Jalali.now());
    dayName.text = weeklyDayName(Jalali.now().weekDay);
    longDate.text = replaceToLondDate(Jalali.now());
    initialTasks(Jalali.now().toString());

    dateController.addListener(refPage);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      DefaultTabController(
        length: 2,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            title: Text("مدیریت کارهای من"),
            actions: <Widget>[],
            bottom: TabBar(
              controller: _tabController,
//            tabs: _tabList,
              tabs: <Widget>[
                Tab(text: "فعالیت های روزانه"),
                Tab(text: "برنامه من"),
              ],
              indicatorColor: Colors.white,
//                controller: _tabController,
            ),
          ),
          floatingActionButton: _inEventTab
              ? PreferredSize(
                  child: newEventForm(),
                  preferredSize: Size(double.infinity, 10),
                )
              : Container(),
          drawer: MyDrawer(),
          body: TabBarView(
              controller: _tabController,
              children: [showTasks(), showEvents()]),
        ),
      )
    ]);
  }

  Future initialTasks(String date) async {
    var result = await db.getAllTasks();
    var resultDone = await db.getAllTaskDoneByDate(replaceDate(toJalali(date)));
    var resultEvent = await db.getAllEventByDate(replaceDate(toJalali(date)));

    List<TaskDone> dones;

    if (resultDone != null && resultDone.length > 0)
      dones = resultDone.map((item) => TaskDone.fromMap(item)).toList();

    List<Event> e = List();

    if (resultEvent != null && resultEvent.length > 0) {
      e = resultEvent.map((item) => Event.fromMap(item)).toList();
    }

    setState(() {
      events = e;
    });

    if (result != null && result.length > 0) {
      var t = result.map((item) => Task.fromMap(item)).toList();

      if (dones != null && dones.length > 0) {
        t.forEach((i) {
          var d = dones.where((w) => w.TaskId == i.TaskId).toList();
          if (d != null && d.length > 0)
            i.dones = true;
          else
            i.dones = false;
        });
      } else {
        t.forEach((i) {
          i.dones = false;
        });
      }

      t.forEach((item) {
        if (!categories.any((w) => w.CatId == item.CatId))
          setState(() {
            categories.add(Category(CatId: item.CatId, CatName: item.CatName));
          });
      });

      setState(() {
        tasks = t;
        tasks.sort((a, b) => a.ScheduleRepeat.compareTo(b.ScheduleRepeat));
      });
    }
  }

  String replaceDate(Jalali jalali) {
    var date = jalali.toString().split("/");
    date[1] = date[1].length == 1 ? "0" + date[1] : date[1];
    date[2] = date[2].length == 1 ? "0" + date[2] : date[2];

    var resdate = "";
    for (var i = 0; i < date.length; i++) {
      resdate += date[i];
      if (i < date.length - 1) resdate += "/";
    }

    return resdate;
  }

  String replaceToLondDate(Jalali jalali) {
    var date = jalali.toString().split("/");
    date[1] = getMonthName(int.parse(date[1]));
//    date[2] = date[2].length == 1 ? "0" + date[2] : date[2];

    var resdate = weeklyDayName(jalali.weekDay) + " ";
    for (var i = date.length - 1; i >= 0; i--) {
      resdate += date[i];
      if (i != 0) resdate += " ";
    }

    return resdate;
  }

  String getMonthName(int month) {
    switch (month) {
      case 1:
        {
          return "فروردین";
        }
      case 2:
        {
          return "اردیبهشت";
        }
      case 3:
        {
          return "خرداد";
        }
      case 4:
        {
          return "تیر";
        }
      case 5:
        {
          return "مرداد";
        }
      case 6:
        {
          return "شهریور";
        }
      case 7:
        {
          return "مهر";
        }
      case 8:
        {
          return "آبان";
        }
      case 9:
        {
          return "آذر";
        }
      case 10:
        {
          return "دی";
        }
      case 11:
        {
          return "بهمن";
        }
      default:
        {
          return "اسفند";
        }
    }
  }

  Future taskDone(Task task) async {
    if (!dateController.text.contains(replaceDate(Jalali.now()))) {
      var message;
      if (task.dones)
        message = "این کار از لیست کارهای انجام شده شما خارج شود؟";
      else
        message = "این کار به لیست کارهای انجام شده شما اضافه شود؟";

      var resDialog = await showDialog(
          context: context,
          builder: (BuildContext context) {
            return DialogBox(
              question: message,
              title: "ویرایش فعالیت های روزهای گذشته",
            );
          });
      if (resDialog == null || !resDialog) return;
    }
    TaskDone model =
        TaskDone(DoneDate: dateController.text, TaskId: task.TaskId);

    if (!task.dones) {
      var res = await db.saveTaskDone(model);
      if (res > 0) {
        setState(() {
          task.dones = true;
        });
      }
    } else {
      var res = await db.removeTaskDone(model);
      if (res > 0) {
        setState(() {
          task.dones = false;
        });
      }
    }
  }

  gotoDate(int i) {
    var jdate = toJalali(dateController.text);
//    if (i == 1 && jdate.toString() == Jalali.now().toString()) return;
    var d = Jalali.fromDateTime(jdate.toDateTime().add(Duration(days: i)));

    setState(() {
      dateController.text = replaceDate(d);
    });
  }

  refPage() {
    var newDate = toJalali(dateController.text);
    setState(() {
      longDate.text = replaceToLondDate(newDate);
      dayName.text = weeklyDayName(newDate.weekDay);
    });
    initialTasks(newDate.toString());
  }

  Jalali toJalali(String text) {
    var date = dateController.text.split("/");
    return Jalali(int.parse(date[0]), int.parse(date[1]), int.parse(date[2]));
  }

  showTasks() {
    return Container(
      color: Colors.grey[200],
      padding: EdgeInsets.only(top: 10, left: 5, right: 5, bottom: 10),
      child: ListView(
        children: <Widget>[
          Container(
            margin:
                const EdgeInsets.only(top: 15.0, left: 5, right: 5, bottom: 10),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(5)),
                boxShadow: [boxShadow]),
            child: Row(
              children: <Widget>[
                IconButton(
                    icon: Icon(Icons.arrow_back_ios, color: Colors.grey),
                    onPressed: () => gotoDate(-1)),
                Expanded(
                  child: Builder(builder: (BuildContext context) {
                    return Container(
                      child: TextField(
                          enableInteractiveSelection: false, //
                          decoration: InputDecoration(border: InputBorder.none),
                          textAlign: TextAlign.center,
                          textDirection: TextDirection.rtl,
                          onTap: () {
                            FocusScope.of(context).requestFocus(
                                new FocusNode()); // to prevent opening default keyboard
                            showModalBottomSheet(
                                context: context,
                                builder: (BuildContext context) {
                                  return persianDatePicker;
                                });
                          },
                          controller: longDate,
                          onChanged: (v) {
                            refPage();
                          }),
                    );
                  }),
                ),
                IconButton(
                    icon: Icon(
                      Icons.refresh,
                      color: Colors.blue,
                    ),
                    onPressed: () {
                      setState(() {
                        dateController.text = replaceDate(Jalali.now());
                      });
                    }),
                IconButton(
                    icon: Icon(Icons.arrow_forward_ios, color: Colors.grey),
                    onPressed: () => gotoDate(1))
              ],
            ),
          ),
          Container(
              child: Row(
            children: <Widget>[
              Expanded(
                  child: InkWell(
                onTap: () {
                  setState(() {
                    if (daily == 1)
                      daily = 0;
                    else
                      daily = 1;
                  });
                },
                child: Container(
                    margin: EdgeInsets.only(left: 2.5, right: 2.5),
                    padding: EdgeInsets.all(10),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        boxShadow: [boxShadow]),
                    child: Row(
                      children: <Widget>[
                        daily == 1
                            ? Icon(
                                Icons.check_box,
                                color: Colors.lightGreen,
                              )
                            : Icon(
                                Icons.check_box_outline_blank,
                                color: Colors.grey,
                              ),
                        Expanded(
                            child: Text(
                          "روزانه",
                          textAlign: TextAlign.center,
                        ))
                      ],
                    )),
              )),
              Expanded(
                  child: InkWell(
                onTap: () {
                  setState(() {
                    if (weekly == 7)
                      weekly = 0;
                    else
                      weekly = 7;
                  });
                },
                child: Container(
                    margin: EdgeInsets.only(left: 2.5, right: 2.5),
                    padding: EdgeInsets.all(10),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        boxShadow: [boxShadow]),
                    child: Row(
                      children: <Widget>[
                        weekly == 7
                            ? Icon(
                                Icons.check_box,
                                color: Colors.lightGreen,
                              )
                            : Icon(
                                Icons.check_box_outline_blank,
                                color: Colors.grey,
                              ),
                        Expanded(
                            child: Text(
                          "هفتگی",
                          textAlign: TextAlign.center,
                        ))
                      ],
                    )),
              )),
              Expanded(
                  child: InkWell(
                onTap: () {
                  setState(() {
                    if (mountly == 30)
                      mountly = 0;
                    else
                      mountly = 30;
                  });
                },
                child: Container(
                    margin: EdgeInsets.only(left: 2.5, right: 2.5),
                    padding: EdgeInsets.all(10),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        boxShadow: [boxShadow]),
                    child: Row(
                      children: <Widget>[
                        mountly == 30
                            ? Icon(
                                Icons.check_box,
                                color: Colors.lightGreen,
                              )
                            : Icon(
                                Icons.check_box_outline_blank,
                                color: Colors.grey,
                              ),
                        Expanded(
                            child: Text(
                          "ماهانه",
                          textAlign: TextAlign.center,
                        ))
                      ],
                    )),
              ))
            ],
          )),
          SizedBox(height: 10),
          categoryCards(tasks
              .where((w) =>
                  (daily != 0 && w.ScheduleRepeat == daily) ||
                  (weekly != 0 && w.ScheduleRepeat == weekly) ||
                  (mountly != 0 && w.ScheduleRepeat == mountly))
              .toList())
        ],
      ),
    );
  }

  showEvents() {
    return Container(
      color: Colors.grey[200],
      padding: EdgeInsets.only(bottom: 65),
      child: ListView(
        padding: EdgeInsets.only(top: 10, left: 5, right: 5),
        children: <Widget>[
          Container(
            margin:
                const EdgeInsets.only(top: 15.0, left: 5, right: 5, bottom: 10),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(5)),
                boxShadow: [boxShadow]),
            child: Row(
              children: <Widget>[
                IconButton(
                    icon: Icon(Icons.arrow_back_ios, color: Colors.grey),
                    onPressed: () => gotoDate(-1)),
                Expanded(
                  child: Builder(builder: (BuildContext context) {
                    return Container(
                      child: TextField(
                          enableInteractiveSelection: false, //
                          decoration: InputDecoration(border: InputBorder.none),
                          textAlign: TextAlign.center,
                          textDirection: TextDirection.rtl,
                          onTap: () {
                            FocusScope.of(context).requestFocus(
                                new FocusNode()); // to prevent opening default keyboard
                            showModalBottomSheet(
                                context: context,
                                builder: (BuildContext context) {
                                  return persianDatePicker;
                                });
                          },
                          controller: longDate,
                          onChanged: (v) {
                            refPage();
                          }),
                    );
                  }),
                ),
                IconButton(
                    icon: Icon(
                      Icons.refresh,
                      color: Colors.blue,
                    ),
                    onPressed: () {
                      setState(() {
                        dateController.text = replaceDate(Jalali.now());
                      });
                    }),
                IconButton(
                    icon: Icon(Icons.arrow_forward_ios, color: Colors.grey),
                    onPressed: () => gotoDate(1))
              ],
            ),
          ),
          SizedBox(height: 10),
          eventCards()
        ],
      ),
    );
  }

  categoryCards(List<Task> tasks) {
    return Column(
      children: List.generate(categories.length, (int index) {
        return tasks.any((w) => w.CatId == categories[index].CatId)
            ? AnimatedContainer(
                duration: Duration(milliseconds: 200),
                margin: EdgeInsets.only(bottom: 10),
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(1),
                    border: Border.all(width: 1, color: Colors.grey),
                    borderRadius: BorderRadius.all(Radius.circular(5))),
                child: ExpansionTile(
                  title: Text(categories[index].CatName),
                  initiallyExpanded: true,
                  children: <Widget>[
//            Container(
//                padding: EdgeInsets.all(10),
//                child: Text(categories[index].CatName)),
                    SizedBox(height: 10),
                    taskCard(tasks
                        .where((w) => w.CatId == categories[index].CatId)
                        .toList())
                  ],
                ),
              )
            : Container();
      }),
    );
  }

  eventCards() {
    return Column(
      children: List.generate(events.length, (int index) {
        return Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(3)),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey,
                    blurRadius: 0.5,
                    spreadRadius: 0.5,
                    offset: Offset(-0.2, 0.4))
              ]),
          margin: EdgeInsets.only(bottom: 5, right: 5, left: 5),
          child: Material(
            borderRadius: BorderRadius.all(Radius.circular(3)),
            color: Colors.white,
            child: InkWell(
              onTap: () =>
                  eventDone(events[index]), //TODO: taskDone(tasks[index]),
              onLongPress: () => longPressDialogEvent(
                  events[index]), //TODO:longPressDialog(tasks[index]),
              child: Container(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          child: AutoSizeText(events[index].EventName,
                              maxLines: 1, minFontSize: 9)),
                      events[index].EventTime != null &&
                              events[index].EventTime.isNotEmpty
                          ? Container(
                              padding: EdgeInsets.all(3),
                              decoration: BoxDecoration(
                                  color: Colors.grey[400],
//                                  border: Border.all(
//                                      width: 0.5, color: Colors.grey),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3))),
                              child: Row(
                                children: <Widget>[
                                  Text(events[index].EventTime,
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12)),
                                  SizedBox(width: 2),
                                  Icon(Icons.alarm_on,
                                      color: Colors.white, size: 15)
                                ],
                              ),
                            )
                          : Container(),
                      SizedBox(width: 5),
                      events[index].EventDone
                          ? Icon(
                              Icons.check_circle,
                              color: Colors.lightGreen,
                            )
                          : Icon(
                              Icons.radio_button_unchecked,
                              color: Colors.grey,
                            )
                    ],
                  )),
            ),
          ),
        );
      }),
    );
  }

  String weeklyDayName(int weeklyDayNumber) {
    switch (weeklyDayNumber) {
      case 1:
        return "شنبه";
      case 2:
        return "یکشنبه";
      case 3:
        return "دوشنبه";
      case 4:
        return "سه شنبه";
      case 5:
        return "چهارشنبه";
      case 6:
        return "پنجشنبه";
      default:
        return "جمعه";
    }
  }

  openDatePicker() {
    FocusScope.of(context)
        .requestFocus(new FocusNode()); // to prevent opening default keyboard
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return persianDatePicker;
        });
  }

  longPressDialog(Task task) async {
    var resDialog = await showDialog(
        context: context,
        builder: (BuildContext context) {
          return ActionsDialogBox(
            actions: <listTileModel>[
              listTileModel(title: "ویرایش", icon: Icon(Icons.edit), result: 2),
              listTileModel(title: "حذف", icon: Icon(Icons.delete), result: 1),
            ],
          );
        });

    if (resDialog == null) return;
    if (resDialog == 1) {
      deleteTask(task);
    }
    if (resDialog == 2) {
      Navigator.push(context,
              MaterialPageRoute(builder: (context) => EditTask(task: task)))
          .then((val) {
        if (val != null && val) {
          initialTasks(dateController.text);
        }
      });
    }
  }

  Future deleteTask(Task task) async {
    var resDialog = await showDialog(
        context: context,
        builder: (BuildContext context) {
          return DialogBox(
            title: "حذف فعالیت",
            question: "آیا از حذف این فعالیت مطمئن هستید؟",
          );
        });

    if (resDialog == null || !resDialog) return;

    var res = await db.deleteTask(task);

    if (res > 0) {
      setState(() {
        tasks.removeWhere((w) => w.TaskId == task.TaskId);
      });
    } else {
      _scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text("خطا در حذف اطلاعات")));
    }
  }

  Future deleteEvent(Event event) async {
    var resDialog = await showDialog(
        context: context,
        builder: (BuildContext context) {
          return DialogBox(
            title: "حذف برنامه",
            question: "آیا از حذف این برنامه مطمئن هستید؟",
          );
        });

    if (resDialog == null || !resDialog) return;
    await notifications.cancel(event.EventId);
    var res = await db.deleteEvent(event);

    if (res > 0) {
      setState(() {
        events.removeWhere((w) => w.EventId == event.EventId);
      });
    } else {
      _scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text("خطا در حذف اطلاعات")));
    }
  }

  Future gotoTomorrow(Event event) async {
    var resDialog = await showDialog(
        context: context,
        builder: (BuildContext context) {
          return DialogBox(
            title: "انتقال به فردا",
            question: "آیا از انتقال این برنامه به فردا اطمینان دارید؟",
          );
        });

    if (resDialog == null || !resDialog) return;

    var olddate = toJalali(dateController.text);
    var newdate =
        Jalali.fromDateTime(olddate.toDateTime().add(Duration(days: 1)));

    setState(() {
      event.EventDate = replaceDate(newdate);
    });
    var res = await db.updateEvent(event);

    if (res > 0) {
      setState(() {
        events.removeWhere((w) => w.EventId == event.EventId);
      });
    } else {
      setState(() {
        event.EventDate = replaceDate(olddate);
      });
      _scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text("خطا در انتقال برنامه")));
    }
  }

  showNewEventFormDialog() async {
    await showDialog(
        context: context,
        child: AlertDialog(
          content: SingleChildScrollView(child: newEventForm()),
          contentPadding: EdgeInsets.all(0),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15))),
        ));
//    return newEventForm();
  }

  newEventForm() {
    return AnimatedContainer(
        duration: Duration(milliseconds: 300),
        margin: EdgeInsets.only(right: 35),
        height: _showNewEventForm
            ? (MediaQuery.of(context).size.height / 4 > 150
                ? 194
                : (MediaQuery.of(context).size.height / 4) + 44)
            : 44,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(3)),
            boxShadow: [boxShadow]),
//        margin: EdgeInsets.only(bottom: 5, right: 10, left: 10, top: 5),
        child: Column(
          children: <Widget>[
            InkWell(
                onTap: () {
                  setState(() {
                    _showNewEventForm = !_showNewEventForm;
                  });
                },
                child: Container(
                    decoration: BoxDecoration(
                        color: Colors.grey[400],
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(3),
                            topRight: Radius.circular(3))),
                    width: double.maxFinite,
                    padding: EdgeInsets.all(10),
                    child: Row(
                      children: <Widget>[
                        Expanded(child: Text("ثبت برنامه جدید")),
                        _showNewEventForm
                            ? Icon(Icons.close)
                            : Icon(Icons.open_in_browser),
                      ],
                    ))),
            AnimatedContainer(
              duration: Duration(milliseconds: 300),
              height: _showNewEventForm
                  ? (MediaQuery.of(context).size.height / 4 > 150
                      ? 150
                      : MediaQuery.of(context).size.height / 4)
                  : 0,
              padding: EdgeInsets.only(left: 5, right: 10, top: 5, bottom: 5),
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  TextField(
                    controller: eventName,
                    decoration: InputDecoration(hintText: "عنوان برنامه"),
                  ),
                  Row(
                    children: <Widget>[
                      Switch(
                        onChanged: (val) => changeReminderChecked(val),
                        value: _reminderChecked,
                      ),
                      Text("یادآوری در ساعت: "),
                      Expanded(
                        child: TextField(
                          controller: timeController,
                          enabled: _reminderChecked,
                          maxLines: 1,
                          onTap: () => _selectTime(),
                        ),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      FlatButton(
                        color: Colors.lightBlue,
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.save, color: Colors.white),
                            SizedBox(width: 5),
                            Text(
                              "ذخیره",
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        ),
                        onPressed: () => saveEvent(),
                      ),
                      SizedBox(width: 10),
//                      FlatButton(
//                        color: Colors.grey[400],
//                        child: Row(
//                          children: <Widget>[Text("انصراف")],
//                        ),
//                        onPressed: () {
//                          Navigator.pop(context);
//                        },
//                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ));
  }

  saveEvent() async {
    try {
      FocusScope.of(context)
          .requestFocus(new FocusNode()); // to prevent opening default keyboard
      var name = eventName.text;
      var time = timeController.text;
      if (name == null || name.trim() == "") {
        _scaffoldKey.currentState.showSnackBar(
            SnackBar(content: Text("عنوان برنامه جدید را وارد نمایید")));
        eventName.text = "";
        return;
      }
      if (_reminderChecked && (time == null || time.trim() == "")) {
        _scaffoldKey.currentState.showSnackBar(
            SnackBar(content: Text("ساعت یادآوری را وارد نمایید")));
        timeController.text = "";
        return;
      }
      var currentDate = toJalali(dateController.text);
      var model = Event(
          EventName: name,
          EventTime: _reminderChecked ? time : "",
          EventDate: replaceDate(currentDate),
          EventDone: false);

      var res = await db.saveEvent(model);

      if (res > 0) {
        final directory = await getApplicationDocumentsDirectory();
        var file = File('${directory.path}/counter.txt');
        model.EventId = res;
        if (_reminderChecked) {
          file.writeAsString('1');
          var dateTime = currentDate.toDateTime().toString();
//        print(dateTime);
          file.writeAsString('2');

          dateTime = dateTime.replaceRange(11, 16, time);
          file.writeAsString('3');

//        print(DateTime.parse(dateTime));
//        print(DateTime.now().add(Duration(seconds: 5)));

          await scheduleNotification(notifications,
              body: name,
//              schedule: DateTime.now().add(Duration(seconds: 5)),
              schedule: DateTime.parse(dateTime),
              id: res,
              title: "یادآور برنامه");

          _scaffoldKey.currentState
              .showSnackBar(SnackBar(content: Text(dateTime)));
        }

        eventName.text = "";
        timeController.text = "";
        selectedTime = null;
        setState(() {
          events.add(model);
        });
      } else {
        _scaffoldKey.currentState
            .showSnackBar(SnackBar(content: Text("خطا در ثبت اطلاعات")));
      }
    } catch (e) {
//      file.writeAsString(e.toString());
      _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(e)));
    }
  }

  Future onSelectNotification(String payload) async => await Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Home()),
      );

  eventDone(Event event) async {
    setState(() {
      event.EventDone = !event.EventDone;
    });
    var res = await db.updateEvent(event);
    if (res <= 0) {
      setState(() {
        event.EventDone = !event.EventDone;
      });
    }
  }

  longPressDialogEvent(Event event) async {
    var resDialog = await showDialog(
        context: context,
        builder: (BuildContext context) {
          return ActionsDialogBox(
            actions: <listTileModel>[
              listTileModel(
                  title: "فردا انجام می دهم",
                  icon: Icon(Icons.forward),
                  result: 2),
              listTileModel(
                  title: "تغییر یادآور", icon: Icon(Icons.alarm_on), result: 3),
              listTileModel(
                  title: "حذف یادآور", icon: Icon(Icons.alarm_off), result: 4),
              listTileModel(title: "حذف", icon: Icon(Icons.delete), result: 1),
            ],
          );
        });

    if (resDialog == null) return;
    if (resDialog == 1) {
      deleteEvent(event);
    }
    if (resDialog == 2) {
      gotoTomorrow(event);
    }
    if (resDialog == 3) {
      changeAlarmTime(event);
    }
    if (resDialog == 4) {
      deleteAlarm(event);
    }
  }

  taskCard(List<Task> tasks) {
    return Container(
      child: Column(
        children: List.generate(tasks.length, (int index) {
          return Container(
            decoration: BoxDecoration(
                border:
                    Border(bottom: BorderSide(width: 0.5, color: Colors.grey))
//              borderRadius: BorderRadius.all(Radius.circular(0)),
//                boxShadow: [
//                  BoxShadow(
//                      color: Colors.grey[400],
//                      blurRadius: 0.5,
//                      spreadRadius: 0.5,
//                      offset: Offset(-0.2, 0.4))
//                ]
                ),
            margin: EdgeInsets.only(),
            child: Material(
              borderRadius: BorderRadius.all(Radius.circular(3)),
              color: Colors.white,
              child: InkWell(
                onTap: () => taskDone(tasks[index]),
                onLongPress: () => longPressDialog(tasks[index]),
                child: Container(
                    padding: EdgeInsets.all(10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: AutoSizeText(tasks[index].Name,
                                maxLines: 1, minFontSize: 6)),
                        Text(
                            tasks[index].ScheduleRepeat == 1
                                ? "روزانه"
                                : tasks[index].ScheduleRepeat == 7
                                    ? "هفتگی"
                                    : "ماهانه",
                            style: TextStyle(color: Colors.grey)),
                        SizedBox(width: 5),
                        tasks[index].dones
                            ? Icon(
                                Icons.check_circle,
                                color: Colors.lightGreen,
                              )
                            : Icon(
                                Icons.radio_button_unchecked,
                                color: Colors.grey,
                              )
                      ],
                    )),
              ),
            ),
          );
        }),
      ),
    );
  }

  changeReminderChecked(bool val) {
    setState(() {
      _reminderChecked = !_reminderChecked;
    });
  }

  TimeOfDay selectedTime;
//  ValueChanged<TimeOfDay> selectTime;

  Future<void> _selectTime() async {
    if (selectedTime == null) selectedTime = TimeOfDay.now();
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: selectedTime);
    if (picked != null) {
      setState(() {
//        selectTime(picked);
        selectedTime = picked;
        timeController.text =
            toTwoString(picked.hour) + ":" + toTwoString(picked.minute);
      });
    }
  }

  String toTwoString(int num) {
    if (num == null) return "";
    String res = num.toString();
    if (res.length == 1) res = "0" + res;
    return res;
  }

  void changeAlarmTime(Event event) async {
    var time = await getTime();
    if (time == null) return;
    var newTime = toTwoString(time.hour) + ":" + toTwoString(time.minute);
    var dateTime = toJalali(dateController.text).toDateTime().toString();
    dateTime = dateTime.replaceRange(11, 16, newTime.toString());

    await notifications.cancel(event.EventId);

    await scheduleNotification(notifications,
        body: event.EventName,
//            schedule: DateTime.now().add(Duration(seconds: 5)),
        schedule: DateTime.parse(dateTime),
        id: event.EventId,
        title: "یادآور برنامه");
    event.EventTime = newTime;
    await db.updateEvent(event);

    await initialTasks(dateController.text);
  }

  void deleteAlarm(Event event) async {
    await notifications.cancel(event.EventId);

    event.EventTime = "";
    await db.updateEvent(event);

    await initialTasks(dateController.text);
  }

  Future<TimeOfDay> getTime() async {
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: TimeOfDay.now());
    return picked;
  }

//  Future<bool> checkPermission() async {
//    try {
//      if (Platform.isAndroid) {
//        Map<PermissionGroup, PermissionStatus> permissions =
//            await PermissionHandler().requestPermissions(
//                [PermissionGroup.sensors, PermissionGroup.reminders]);
//        if (permissions[PermissionGroup.unknown] == PermissionStatus.granted &&
//            permissions[PermissionGroup.sensors] == PermissionStatus.granted &&
//            permissions[PermissionGroup.reminders] ==
//                PermissionStatus.granted) {
//          return true;
//        } else {
//          FlutterError("لطفا دسترسی لازم را اعمال نمایید");
//          return false;
//        }
//      } else if (Platform.isIOS) {}
//
//      return false;
//    } catch (ex) {
//      return false;
//    }
//  }
}
