import 'package:dailyscheduler/database/DBHelper.dart';
import 'package:dailyscheduler/model/Category.dart';
import 'package:dailyscheduler/model/Task.dart';
import 'package:dailyscheduler/ui/Categories.dart';
import 'package:dailyscheduler/ui/Component/DialogBox.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class RegisterNewTask extends StatefulWidget {
  @override
  _RegisterNewTaskState createState() => _RegisterNewTaskState();
}

class _RegisterNewTaskState extends State<RegisterNewTask> {
  final _border = OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(5)),
      gapPadding: 0,
      borderSide: BorderSide(
          color: Colors.grey[100], width: 0.5, style: BorderStyle.solid));

  final taskTitle = TextEditingController();
  final goal = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _autoValidation = false;
  bool _isSaving = false;
  DbHelper db;
  var cycles = new List<DropdownMenuItem>();
  DropdownMenuItem SelectedVal;
  var categories = new List<Category>();
  int SelectedCat;
  @override
  void initState() {
    super.initState();

    cycles.add(new DropdownMenuItem(child: new Text("روزانه"), value: 1));
    cycles.add(new DropdownMenuItem(child: new Text("هفتگی"), value: 7));
    cycles.add(new DropdownMenuItem(child: new Text("ماهانه"), value: 30));

    categories = List();
    SelectedCat = 0;

    initialCategories();

    setState(() {
      SelectedVal = cycles[0];
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      builder: (BuildContext context, Widget child) {
        return Directionality(
            textDirection: TextDirection.rtl,
            child: Builder(builder: (BuildContext context) {
              return MediaQuery(
                  data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                  child: child);
            }));
      },
      theme: ThemeData(fontFamily: 'BYekan'),
      home: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("ثبت کار جدید"),
          leading: IconButton(
              icon: Icon(Icons.arrow_back), onPressed: () => _onWillPop()),
        ),
        body: ModalProgressHUD(
          inAsyncCall: _isSaving,
          child: Container(
              color: Colors.grey[300],
              child: Form(
                key: _formKey,
                autovalidate: _autoValidation,
                child: Container(
                  margin: EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey,
                            blurRadius: 0.5,
                            spreadRadius: 0.5,
                            offset: Offset(-0.2, 0.4))
                      ]),
                  child: ListView(
                    children: <Widget>[
                      TextFormField(
                        maxLines: 1,
                        controller: taskTitle,
                        decoration: InputDecoration(
                            labelText: "عنوان کار",
                            border: _border,
                            prefixIcon: Icon(Icons.work)),
                        validator: (val) {
                          if (val == null || val == "")
                            return "عنوان را وارد نمایید";
                        },
                      ),
                      SizedBox(height: 15),
                      Text(
                        "دوره تکرار",
                        style: TextStyle(color: Colors.grey, fontSize: 14),
                      ),
                      DropdownButton(
                        isExpanded: true,
                        items: cycles,
                        onChanged: (val) {
                          setState(() {
                            SelectedVal =
                                cycles.where((w) => w.value == val).first;
                          });
                        },
                        value: SelectedVal.value,
                      ),
                      SizedBox(height: 15),
                      Text(
                        "دسته بندی",
                        style: TextStyle(color: Colors.grey, fontSize: 14),
                      ),
                      categories == null || categories.length == 0
                          ? Container()
                          : DropdownButton(
                              isExpanded: true,
                              items: categories.map((Category val) {
                                return DropdownMenuItem(
                                  value: val.CatId,
                                  child: Text(val.CatName),
                                );
                              }).toList(),
                              onChanged: (val) {
                                setState(() {
                                  SelectedCat = val;
                                });
                              },
                              value: SelectedCat,
                            ),
                      SizedBox(height: 15),
                      TextFormField(
                        maxLines: 1,
                        controller: goal,
                        decoration: InputDecoration(
                          labelText: "هدف در ماه",
                          border: _border,
                          prefixIcon: Icon(Icons.check_circle),
                        ),
                        validator: (val) {
                          if (val == null || val == "")
                            return "هدف را تایین نمایید";
                          if (!isNumeric(val)) return "هدف باید عددی باشد";
                          if (int.parse(val) > 30 || int.parse(val) < 1)
                            return "هدف باید بین 1 تا 30 باشد";
                        },
                      ),
                      SizedBox(height: 15),
                      Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                                  margin: EdgeInsets.only(left: 5, right: 5),
                                  child: RaisedButton(
                                      onPressed: () => saveForm(),
                                      color: Colors.lightGreen,
                                      child: Text("تایید و ثبت")))),
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(left: 5, right: 5),
                              child: RaisedButton(
                                onPressed: () => _onWillPop(),
                                color: Colors.redAccent,
                                child: Text("انصراف"),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              )),
        ),
      ),
    );
  }

  _onWillPop() {
    return Navigator.pop(context);
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.parse(s, (e) => null) != null;
  }

  saveForm() async {
    if (!_formKey.currentState.validate()) {
      setState(() {
        _autoValidation = true;
      });
      return;
    }

    setState(() {
      _isSaving = true;
    });

    db = DbHelper();

    var model = Task(
        Name: taskTitle.text,
        GoalCount: int.parse(goal.text),
        ScheduleRepeat: SelectedVal.value,
        CatId: SelectedCat);

    var res = await db.saveTask(model);
    var message = "";
    if (res > 0)
      message = "ثبت با موفقیت انجام شد";
    else
      message = "خطا در ثبت فرم";

    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message)));

    setState(() {
      if (res > 0) {
        taskTitle.text = "";
        goal.text = "";
      }
      _autoValidation = false;
      _isSaving = false;
    });
  }

  Future initialCategories() async {
    if (db == null) db = DbHelper();

    var res = await db.getCategory();
    if (res == null || res.length <= 0) {
      var val = await showDialog(
          context: context,
          child: DialogBox(
            question:
                "دسته بندی ای ثبت نشده است، ابتدا باید دسته بندی ثبت کنید",
            buttons: <Widget>[
              FlatButton(
                  child: Text("باشه"),
                  onPressed: () {
                    Navigator.pop(context, true);
                  })
            ],
          ));
      if (val == null || val) {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => Categories()));
      }
    }
    var tCat = res.map((item) => Category.fromMap(item)).toList();

    setState(() {
      categories = tCat;
      SelectedCat = tCat.first.CatId;
    });
  }
}
