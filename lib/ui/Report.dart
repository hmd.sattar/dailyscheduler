import 'package:auto_size_text/auto_size_text.dart';
import 'package:dailyscheduler/database/DBHelper.dart';
import 'package:dailyscheduler/model/Task.dart';
import 'package:dailyscheduler/model/TaskDone.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:shamsi_date/shamsi_date.dart';

class Report extends StatefulWidget {
  @override
  _ReportState createState() => _ReportState();
}

class _ReportState extends State<Report> {
  TextEditingController daysTextController = TextEditingController();
  TextEditingController mountsTextController = TextEditingController();
  TextEditingController yearsTextController = TextEditingController();

  List<Task> tasks = List();
  List<DropdownMenuItem> mounts = List<DropdownMenuItem>();
  DropdownMenuItem selectedMount;

  DbHelper db;

  @override
  void initState() {
    super.initState();

    db = DbHelper();

    var date = Jalali.now();
//    daysTextController.text = date.day.toString();
//    mountsTextController.text = getMonthName(date.month);
    yearsTextController.text = date.year.toString();

    initialTasks(date.toString());

    for (var i = 1; i <= 12; i++)
      mounts.add(DropdownMenuItem(child: Text(getMonthName(i)), value: i));

    selectedMount = mounts.firstWhere((w) => w.value == date.month);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      builder: (BuildContext context, Widget child) {
        return Directionality(
            textDirection: TextDirection.rtl,
            child: Builder(builder: (BuildContext context) {
              return MediaQuery(
                  data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                  child: child);
            }));
      },
      theme: ThemeData(fontFamily: 'BYekan'),
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
//        key: _scaffoldKey,
          appBar: AppBar(
              title: Text("گزارش فعالیت ها"),
              leading: IconButton(
                  icon: Icon(Icons.arrow_back), onPressed: () => _onWillPop()),
              bottom: TabBar(
//            tabs: _tabList,
                tabs: <Widget>[
                  Tab(text: "روزانه"),
                  Tab(text: "هفتگی"),
                  Tab(text: "ماهانه"),
                  Tab(text: "گزارش کلی")
                ],
              )),
          body: ModalProgressHUD(
            inAsyncCall: false,
            child: Scrollbar(
                child: TabBarView(children: [
              showTaskReport(1),
              showTaskReport(7),
              showTaskReport(30),
              showTaskReport(0)
            ])),
          ),
        ),
      ),
    );
  }

  _onWillPop() {
    return Navigator.pop(context);
  }

  String getMonthName(int month) {
    switch (month) {
      case 1:
        {
          return "فروردین";
        }
      case 2:
        {
          return "اردیبهشت";
        }
      case 3:
        {
          return "خرداد";
        }
      case 4:
        {
          return "تیر";
        }
      case 5:
        {
          return "مرداد";
        }
      case 6:
        {
          return "شهریور";
        }
      case 7:
        {
          return "مهر";
        }
      case 8:
        {
          return "آبان";
        }
      case 9:
        {
          return "آذر";
        }
      case 10:
        {
          return "دی";
        }
      case 11:
        {
          return "بهمن";
        }
      default:
        {
          return "اسفند";
        }
    }
  }

  Jalali toJalali(String text) {
    var date = text.split("/");
    return Jalali(int.parse(date[0]), int.parse(date[1]), int.parse(date[2]));
  }

  String replaceDate(Jalali jalali) {
    var date = jalali.toString().split("/");
    date[1] = date[1].length == 1 ? "0" + date[1] : date[1];
    date[2] = date[2].length == 1 ? "0" + date[2] : date[2];

    var resdate = "";
    for (var i = 0; i < date.length; i++) {
      resdate += date[i];
      if (i < date.length - 1) resdate += "/";
    }

    return resdate;
  }

  String changeDayDate(Jalali jalali, String day) {
    var date = jalali.toString().split("/");
    date[1] = date[1].length == 1 ? "0" + date[1] : date[1];
    date[2] = day.length == 1 ? "0" + day : day;

    var resdate = "";
    for (var i = 0; i < date.length; i++) {
      resdate += date[i];
      if (i < date.length - 1) resdate += "/";
    }

    return resdate;
  }

  Future initialTasks(String date) async {
    var jalDate = toJalali(date);
    var fromDate = changeDayDate(jalDate, "01");
    var toDate = changeDayDate(jalDate, jalDate.monthLength.toString());

    var result = await db.getAllTasks();
    var resultDone = await db.getAllTaskDoneByDateToDate(fromDate, toDate);

    List<TaskDone> dones;

    if (resultDone != null && resultDone.length > 0)
      dones = resultDone.map((item) => TaskDone.fromMap(item)).toList();

    if (result != null && result.length > 0) {
      var t = result.map((item) => Task.fromMap(item)).toList();

      if (dones != null && dones.length > 0) {
        t.forEach((i) {
          var d = dones.where((w) => w.TaskId == i.TaskId).toList();
          if (d != null && d.length > 0) {
            i.dones = true;
            i.DoneCount = d.length;
            i.DonePercentage = (i.DoneCount / i.GoalCount * 100).round();
          } else {
            i.dones = false;
            i.DoneCount = 0;
            i.DonePercentage = 0;
          }
        });
      } else {
        t.forEach((i) {
          i.dones = false;
          i.DoneCount = 0;
          i.DonePercentage = 0;
        });
      }
      setState(() {
        tasks = t;
      });
    }
  }

  taskCards(List<Task> tasks) {
    return Column(
      children: List.generate(tasks.length + 1, (int index) {
        return index == 0
            ? Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(3)),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey,
                                blurRadius: 0.5,
                                spreadRadius: 0.5,
                                offset: Offset(-0.2, 0.4))
                          ]),
                      margin: EdgeInsets.only(bottom: 0),
                      child: Material(
//                        borderRadius: BorderRadius.all(Radius.circular(3)),
                        color: Colors.grey[300],
                        child: InkWell(
//                    onTap: () => taskDone(tasks[index]),
                          child: Container(
                              padding: EdgeInsets.all(10),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                      flex: 5,
                                      child: AutoSizeText(
                                        "نام کار",
                                        maxLines: 1,
                                        minFontSize: 10,
                                      )),
                                  Expanded(
                                      flex: 1,
                                      child: AutoSizeText("هدف",
                                          maxLines: 1,
                                          minFontSize: 10,
                                          textAlign: TextAlign.center)),
                                  Expanded(
                                      flex: 1,
                                      child: AutoSizeText("انجام شده",
                                          maxLines: 1,
                                          minFontSize: 10,
                                          textAlign: TextAlign.center)),
                                  Expanded(
                                      flex: 1,
                                      child: AutoSizeText("درصد",
                                          maxLines: 1,
                                          minFontSize: 10,
                                          textAlign: TextAlign.center))
                                ],
                              )),
                        ),
                      ),
                    ),
                  )
                ],
              )
            : Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
//                          borderRadius: BorderRadius.all(Radius.circular(3)),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey,
                                blurRadius: 0.5,
                                spreadRadius: 0.5,
                                offset: Offset(-0.2, 0.4))
                          ]),
                      margin: EdgeInsets.only(bottom: 1),
                      child: Material(
//                        borderRadius: BorderRadius.all(Radius.circular(3)),
                        color: Colors.white,
                        child: InkWell(
//                    onTap: () => taskDone(tasks[index]),
                          child: Container(
                              padding: EdgeInsets.all(10),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                      flex: 5,
                                      child: AutoSizeText(
                                        tasks[index - 1].Name,
                                        maxLines: 1,
                                        minFontSize: 12,
                                      )),
                                  Expanded(
                                      flex: 1,
                                      child: AutoSizeText(
                                          tasks[index - 1].GoalCount.toString(),
                                          maxLines: 1,
                                          minFontSize: 10,
                                          textAlign: TextAlign.center)),
                                  Expanded(
                                      flex: 1,
                                      child: AutoSizeText(
                                          tasks[index - 1].DoneCount.toString(),
                                          maxLines: 1,
                                          minFontSize: 10,
                                          textAlign: TextAlign.center)),
                                  Expanded(
                                    flex: 1,
                                    child: AutoSizeText(
                                        tasks[index - 1]
                                                .DonePercentage
                                                .toString() +
                                            "%",
                                        maxLines: 1,
                                        minFontSize: 10,
                                        textAlign: TextAlign.center),
                                  )
                                ],
                              )),
                        ),
                      ),
                    ),
                  )
                ],
              );
      }),
    );
  }

  showTaskReport(int i) {
    return ListView(
      children: <Widget>[
        Container(
          margin:
              const EdgeInsets.only(top: 15.0, left: 5, right: 5, bottom: 10),
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(5)),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey,
                    blurRadius: 0.5,
                    spreadRadius: 0.5,
                    offset: Offset(-0.2, 0.4))
              ]),
          child: Row(
            children: <Widget>[
              Expanded(
                  child: DropdownButton(
                isExpanded: true,
                items: mounts,
                onChanged: (val) {
                  setState(() {
                    selectedMount = mounts.firstWhere((w) => w.value == val);
                    refTasks();
                  });
                },
                value: selectedMount.value,
              )),
              Expanded(
                  child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    IconButton(
                        icon: Icon(Icons.arrow_back_ios, size: 20),
                        onPressed: () => changeYear(-1)),
                    Text(yearsTextController.text),
                    IconButton(
                        icon: Icon(Icons.arrow_forward_ios, size: 20),
                        onPressed: () => changeYear(1)),
                  ],
                ),
              )),
            ],
          ),
        ),
        Container(
            padding: EdgeInsets.all(5),
            child: i == 0
                ? taskCards(tasks)
                : taskCards(tasks.where((w) => w.ScheduleRepeat == i).toList()))
      ],
    );
  }

  changeYear(int i) {
    if (i == 1 && int.parse(yearsTextController.text) == Jalali.now().year)
      return;
    setState(() {
      yearsTextController.text =
          (int.parse(yearsTextController.text) + i).toString();
    });
    refTasks();
  }

  refTasks() {
    var date =
        yearsTextController.text + "/" + selectedMount.value.toString() + "/01";
    initialTasks(date);
  }
}
